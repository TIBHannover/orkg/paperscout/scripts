import json
import os
from typing import Any

from tqdm import tqdm


times = 0
file_num = 9
output_path = "./core"
num_files_written = 3800


def recursively_replace_nulls_with_empty_dicts(input_value: Any) -> Any:
    """
    Recursively replace all null values with empty dicts
    This is needed for the mappings to work
    :param input_value: any value.
    :return: the new modified value.
    """
    if isinstance(input_value, list):
        return [recursively_replace_nulls_with_empty_dicts(element) for element in input_value]
    elif isinstance(input_value, dict):
        return {key: recursively_replace_nulls_with_empty_dicts(value) for key, value in input_value.items()}
    elif input_value is None:
        return {}
    else:
        return input_value


def concat_content_of_file() -> None:
    """
    Concatenate the content of the file with the knowledge-graph.nt file
    :return: Nothing
    """
    global file_num
    global times
    global output_path
    with open(f"{output_path}{str(file_num)}.nt", "a+") as destination, open("knowledge-graph.nt", "r") as source:
        destination.write(source.read())
        destination.write("\n")
    times += 1
    if times >= num_files_written:
        file_num += 1
        times = 0


files = [f"./json/{file}" for file in os.listdir("./json")]
files = sorted(files, key=os.path.getsize)
for file in tqdm(files, unit="files"):
    if file.endswith(".json"):
        file_path = file
        with open(file_path, "r") as f:
            print(f"Processing {file}")
            content = f.read()
            with open("research_papers.json", "w") as tmp_file:
                content = content.strip()
                content = f"[{content}" if content[0] != "[" else content
                content = f"{content}]" if content[-1] != "]" else content
                if content[-4:-1] == ",\n,":
                    content = content[:-4] + content[-1]
                else:
                    content = content.replace("\n", ",\n")
                    content = content.replace(",\n]", "\n]")
                try:
                    new_content = recursively_replace_nulls_with_empty_dicts(json.loads(content))
                    tmp_file.write(json.dumps(new_content))
                except:
                    continue
            try:
                del content
                del new_content
                os.popen("python -m morph_kgc morph-kgc-config.ini").read()
                concat_content_of_file()
                # delete the file
                os.remove(file_path)
            except:
                continue


