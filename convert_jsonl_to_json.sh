#!/bin/bash

# Check if a path argument was provided
if [ -z "$1" ]; then
    echo "Usage: $0 <directory_path>"
    exit 1
fi

# Set the directory path from the command line argument
directory_path="$1"

# Check if the provided path is a directory
if [ ! -d "$directory_path" ]; then
    echo "Error: '$directory_path' is not a valid directory."
    exit 1
fi

# Loop through all JSONL files in the specified directory
for file in "$directory_path"/*.jsonl; do
    # Check if JSONL file exists
    if [ -e "$file" ]; then
        # Construct the output JSON file name
        output_file="${file%.jsonl}.json"

        # Apply the transformation and write to the output JSON file
        (echo -n "["; tr '\n' ',' < "$file" | sed 's/,$//'; echo "]") > "$output_file"
        #awk 'BEGIN { ORS="," } { print $0 } END { sub(/,$/, ""); print "\n" }' "$file" > "$output_file"

        # Remove the original JSONL file
        rm "$file"

        echo "Converted $file to $output_file and deleted $file"
    fi
done
