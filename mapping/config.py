from typing import Optional


class Config:
    ##########################
    # General Parser Config
    ##########################
    base_url: str = "https://paperscout.orkg.org/"
    input_file: str = "../input.json"
    process_num: int = 10
    output_format: str = "turtle"

    ##########################
    # Paper Parser Config
    ##########################
    paper_id_field: str = "coreId"
    paper_class: str = "Paper"

    ##########################
    # DOI Parser Config
    ##########################
    doi_field: str = "doi"
    doi_predicate: str = "doi"
    doi_datatype: Optional[str] = None

    ##########################
    # OAI Parser Config
    ##########################
    oai_field: str = "oai"
    oai_predicate: str = "oai"
    oai_datatype: Optional[str] = None

    ##########################
    # Title Parser Config
    ##########################
    title_field: str = "title"
    title_predicate: str = "title"
    title_datatype: Optional[str] = None

    ##########################
    # datePublished Parser Config
    ##########################
    date_published_field: str = "datePublished"
    date_published_predicate: str = "datePublished"
    date_published_datatype: Optional[str] = "xsd:datetime"

    ##########################
    # Abstract Parser Config
    ##########################
    abstract_field: str = "abstract"
    abstract_predicate: str = "abstract"
    abstract_datatype: Optional[str] = None

    ##########################
    # Year Parser Config
    ##########################
    year_field: str = "year"
    year_predicate: str = "year"
    year_datatype: Optional[str] = "xsd:gYear"

    ##########################
    # ISSN Parser Config
    ##########################
    issn_field: str = "issn"
    issn_predicate: str = "issn"
    issn_datatype: Optional[str] = None

    ##########################
    # FullText Parser Config
    ##########################
    full_text_field: str = "fullText"
    full_text_predicate: str = "fullText"
    full_text_datatype: Optional[str] = None

    ##########################
    # Subjects Parser Config
    ##########################
    subjects_field: str = "subjects"
    subjects_predicate: str = "subject"
    subjects_class: str = "Subject"

    ##########################
    # URLs Parser Config
    ##########################
    urls_field: str = "urls"
    urls_predicate: str = "url"
    urls_datatype: Optional[str] = "xsd:anyURI"

    ##########################
    # Identifiers Parser Config
    ##########################
    identifiers_field: str = "identifiers"
    identifiers_predicate: str = "identifier"
    identifiers_datatype: Optional[str] = None

    ##########################
    # Topics Parser Config
    ##########################
    topics_field: str = "topics"
    topics_predicate: str = "topic"
    topics_class: str = "Topic"

    ##########################
    # DownloadUrl Parser Config
    ##########################
    download_url_field: str = "downloadUrl"
    download_url_predicate: str = "downloadUrl"
    download_url_datatype: Optional[str] = "xsd:anyURI"

    ##########################
    # fullTextIdentifiers Parser Config
    ##########################
    full_text_id_field: str = "fullTextIdentifier"
    full_text_id_predicate: str = "fullTextIdentifier"
    full_text_id_datatype: Optional[str] = "xsd:anyURI"

    ##########################
    # PDF Hash Value Parser Config
    ##########################
    pdf_hash_value_field: str = "pdfHashValue"
    pdf_hash_value_predicate: str = "pdfHashValue"
    pdf_hash_value_datatype: Optional[str] = None

    ##########################
    # rawRecordXml Parser Config
    ##########################
    raw_record_xml_field: str = "rawRecordXml"
    raw_record_xml_predicate: str = "rawRecordXml"
    raw_record_xml_datatype: Optional[str] = None

    ##########################
    # Journals Parser Config
    ##########################
    journals_field: str = "journals"
    journals_predicate: str = "journal"
    journals_class: str = "Journal"

    journals_identifier_field: str = "identifiers"
    journals_title_field: str = "title"

    ##########################
    # Authors Parser Config
    ##########################
    authors_field: str = "authors"
    authors_predicate: str = "author"
    authors_class: str = "Author"

    ##########################
    # Enrichments Parser Config
    ##########################
    enrichments_field: str = "enrichments"
    enrichments_predicate: str = "enrichment"
    enrichments_class: str = "Enrichment"
    doc_type_predicate: str = "documentType"
    doc_type_field: str = "documentType"
    doc_type_class: str = "DocumentType"
    references_field: str = "references"
    references_predicate: str = "reference"
    references_datatype: Optional[str] = None
    type_field: str = "type"
    type_predicate: str = "type"
    type_datatype: Optional[str] = None
    confidence_field: str = "confidence"
    confidence_predicate: str = "confidence"
    confidence_datatype: Optional[str] = None
    citation_count_field: str = "citationCount"
    citation_count_predicate: str = "citationCount"
    citation_count_datatype: Optional[str] = "xsd:integer"

    ##########################
    # Language Parser Config
    ##########################
    language_field: str = "language"
    language_predicate: str = "language"
    language_class: str = "DocumentLanguage"
    language_code_field: str = "code"
    language_code_predicate: str = "code"
    language_code_datatype: Optional[str] = None
    language_name_field: str = "name"
    language_name_predicate: str = "name"
    language_name_datatype: Optional[str] = None

    ##########################
    # Publisher Parser Config
    ##########################
    publisher_field: str = "publisher"
    publisher_predicate: str = "publisher"
    publisher_class: str = "Publisher"

    ##########################
    # Relations Parser Config
    ##########################
    relations_field: str = "relations"
    relations_predicate: str = "relation"
    relations_datatype: Optional[str] = "xsd:anyURI"

    ##########################
    # Contributors Parser Config
    ##########################
    contributors_field: str = "contributors"
    contributors_predicate: str = "contributor"
    contributors_datatype: Optional[str] = None


