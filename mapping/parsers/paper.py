from typing import Dict, List, Tuple, Set

from rdflib.term import Node, Literal

from mapping.parsers.base import MappingParser, Config


class PaperParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing papers"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing papers"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_type_predicate_node(),
            self.make_class_type_node(Config.paper_class)
        )]

    def _extraction_field(self) -> str:
        return Config.paper_id_field


class DOIParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing DOIs"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing DOIs"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.doi_predicate),
            self.make_literal_node(paper[Config.doi_field], Config.doi_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.doi_field


class OAIParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing OAIs"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing OAIs"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.oai_predicate),
            self.make_literal_node(paper[Config.oai_field], Config.oai_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.oai_field


class TitleParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing Titles"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Titles"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.title_predicate),
            self.make_literal_node(paper[Config.title_field], Config.title_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.title_field


class DatePublishedParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing DatePublished"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing DatePublished"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.date_published_predicate),
            self.make_literal_node(paper[Config.date_published_field], Config.date_published_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.date_published_field


class AbstractParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing Abstracts"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Abstracts"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.abstract_predicate),
            self.make_literal_node(paper[Config.abstract_field], Config.abstract_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.abstract_field


class YearParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing Years"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Years"

    @staticmethod
    def _make_year_node(year: int) -> Literal:
        if 1700 < year < 9999:
            return Literal(year, datatype=Config.year_datatype)
        else:
            return Literal(str(year))

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.year_predicate),
            self._make_year_node(paper[Config.year_field])
        )]

    def _extraction_field(self) -> str:
        return Config.year_field


class ISSNParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing ISSNs"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing ISSNs"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.issn_predicate),
            self.make_literal_node(paper[Config.issn_field], Config.issn_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.issn_field


class FullTextParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing FullTexts"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing FullTexts"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.full_text_predicate),
            self.make_literal_node(paper[Config.full_text_field], Config.full_text_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.full_text_field


class SubjectsParser(MappingParser):
    subjects_set: Set = set()

    def _start_parsing_message(self) -> str:
        return f"Started parsing Subjects"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Subjects"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        for subject in paper[Config.subjects_field]:
            triples += [
                # Add connection to paper
                (
                    self.make_paper_subject(paper[Config.paper_id_field]),
                    self.make_predicate_node(Config.subjects_predicate),
                    self.make_object_node(f"subject-{abs(hash(subject))}")
                )]
            if subject not in self.subjects_set:
                triples += [
                    # Add label to subject
                    (
                        self.make_object_node(f"subject-{abs(hash(subject))}"),
                        self.make_label_predicate_node(),
                        self.make_literal_node(subject)
                    ),
                    # Add type to subject
                    (
                        self.make_object_node(f"subject-{abs(hash(subject))}"),
                        self.make_type_predicate_node(),
                        self.make_class_type_node(Config.subjects_class)
                    )
                ]
                self.subjects_set.add(subject)

        return triples

    def _extraction_field(self) -> str:
        return Config.subjects_field


class URLsParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing URLs"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing URLs"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.urls_predicate),
            self.make_literal_node(url, Config.urls_datatype)
        ) for url in paper[Config.urls_field]]

    def _extraction_field(self) -> str:
        return Config.urls_field


class IdentifiersParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing Identifiers"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Identifiers"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.identifiers_predicate),
            self.make_literal_node(identifier, Config.identifiers_datatype)
        ) for identifier in paper[Config.identifiers_field]]

    def _extraction_field(self) -> str:
        return Config.identifiers_field


class TopicsParser(MappingParser):
    topics_set: Set = set()

    def _start_parsing_message(self) -> str:
        return f"Started parsing Topics"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Topics"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        for topic in paper[Config.topics_field]:
            triples += [
                # Add connection to paper
                (
                    self.make_paper_subject(paper[Config.paper_id_field]),
                    self.make_predicate_node(Config.topics_predicate),
                    self.make_object_node(f"topic-{abs(hash(topic))}")
                )]
            if topic not in self.topics_set:
                triples += [
                    # Add label to subject
                    (
                        self.make_object_node(f"topic-{abs(hash(topic))}"),
                        self.make_label_predicate_node(),
                        self.make_literal_node(topic)
                    ),
                    # Add type to subject
                    (
                        self.make_object_node(f"topic-{abs(hash(topic))}"),
                        self.make_type_predicate_node(),
                        self.make_class_type_node(Config.topics_class)
                    )
                ]
                self.topics_set.add(topic)

        return triples

    def _extraction_field(self) -> str:
        return Config.topics_field


class DownloadURLParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing DownloadURLs"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing DownloadURLs"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.download_url_predicate),
            self.make_literal_node(paper[Config.download_url_field], Config.download_url_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.download_url_field


class FullTextIdentifierParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing FullTextIdentifiers"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing FullTextIdentifiers"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.full_text_id_predicate),
            self.make_literal_node(paper[Config.full_text_id_field], Config.full_text_id_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.full_text_id_field


class PDFHashValueParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing pdfHashValues"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing pdfHashValues"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.pdf_hash_value_predicate),
            self.make_literal_node(paper[Config.pdf_hash_value_field], Config.pdf_hash_value_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.pdf_hash_value_field


class RawRecordXMLParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing rawRecordXml"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing rawRecordXml"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.raw_record_xml_predicate),
            self.make_literal_node(paper[Config.raw_record_xml_field], Config.raw_record_xml_datatype)
        )]

    def _extraction_field(self) -> str:
        return Config.raw_record_xml_field


class JournalsParser(MappingParser):
    journals_set: Set = set()

    def _start_parsing_message(self) -> str:
        return f"Started parsing Journals"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Journals"


    @staticmethod
    def _get_journal_identifier(journal: Dict) -> str:
        if Config.journals_title_field in journal and journal[Config.journals_title_field]:
            return journal[Config.journals_title_field]
        else:
            return journal[Config.journals_identifier_field][0]



    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        for journal in paper[Config.journals_field]:
            triples += [
                # Add connection to paper
                (
                    self.make_paper_subject(paper[Config.paper_id_field]),
                    self.make_predicate_node(Config.journals_predicate),
                    self.make_object_node(f"journal-{abs(hash(self._get_journal_identifier(journal)))}")
                )]
            if self._get_journal_identifier(journal) not in self.journals_set:
                triples += [
                    # Add label to subject
                    (
                        self.make_object_node(f"journal-{abs(hash(self._get_journal_identifier(journal)))}"),
                        self.make_label_predicate_node(),
                        self.make_literal_node(self._get_journal_identifier(journal))
                    ),
                    # Add type to subject
                    (
                        self.make_object_node(f"journal-{abs(hash(self._get_journal_identifier(journal)))}"),
                        self.make_type_predicate_node(),
                        self.make_class_type_node(Config.journals_class)
                    )
                ]
                self.journals_set.add(self._get_journal_identifier(journal))
        return triples

    def _extraction_field(self) -> str:
        return Config.journals_field


class AuthorsParser(MappingParser):
    authors_set: Set = set()

    def _start_parsing_message(self) -> str:
        return f"Started parsing Authors"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Authors"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        for author in paper[Config.authors_field]:
            triples += [
                # Add connection to paper
                (
                    self.make_paper_subject(paper[Config.paper_id_field]),
                    self.make_predicate_node(Config.authors_predicate),
                    self.make_object_node(f"author-{abs(hash(author))}")
                )]
            if author not in self.authors_set:
                triples += [
                    # Add label to subject
                    (
                        self.make_object_node(f"author-{abs(hash(author))}"),
                        self.make_label_predicate_node(),
                        self.make_literal_node(author)
                    ),
                    # Add type to subject
                    (
                        self.make_object_node(f"author-{abs(hash(author))}"),
                        self.make_type_predicate_node(),
                        self.make_class_type_node(Config.authors_class)
                    )
                ]
                self.authors_set.add(author)

        return triples

    def _extraction_field(self) -> str:
        return Config.authors_field


class EnrichmentsParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing Enrichments"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Enrichments"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        enrichments_dict = paper[Config.enrichments_field]
        if any([
            Config.references_field in enrichments_dict,
            Config.doc_type_field in enrichments_dict,
            Config.citation_count_field in enrichments_dict
        ]):
            triples = self._create_enrichment_triple(paper)
            triples += self._add_references_if_present(enrichments_dict, paper)
            triples += self._add_doc_type_if_present(enrichments_dict, paper)
            triples += self._add_citation_count_if_present(enrichments_dict, paper)
        return triples

    def _add_doc_type_if_present(self, enrichments_dict: Dict, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        # Check if documentType is present
        if (Config.doc_type_field in enrichments_dict and enrichments_dict[Config.doc_type_field] and
                (Config.type_field in enrichments_dict[Config.doc_type_field] or
                 Config.confidence_field in enrichments_dict[Config.doc_type_field])):
            triples += self._create_doc_type_triples(paper)
            doc_type_dict = enrichments_dict[Config.doc_type_field]
            triples += self._add_doc_concrete_type_if_present(doc_type_dict, paper)
            triples += self._add_doc_type_confidence_if_present(doc_type_dict, paper)
        return triples

    def _add_citation_count_if_present(self, enrichments_dict: Dict, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        # Check if citationCount is present
        if Config.citation_count_field in enrichments_dict and enrichments_dict[Config.citation_count_field]:
            return [(
                # Add citationCount to enrichment
                self.make_object_node(f"enrichment-{paper[Config.paper_id_field]}"),
                self.make_predicate_node(Config.citation_count_predicate),
                self.make_literal_node(enrichments_dict[Config.citation_count_field], Config.citation_count_datatype)
            )]
        return []

    def _add_doc_type_confidence_if_present(self, doc_type_dict: Dict, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        # Check if confidence is present in documentType
        if Config.confidence_field in doc_type_dict and doc_type_dict[Config.confidence_field]:
            return [(
                # Add confidence to documentType
                self.make_object_node(f"documentType-{paper[Config.paper_id_field]}"),
                self.make_predicate_node(Config.confidence_predicate),
                self.make_literal_node(doc_type_dict[Config.confidence_field], Config.confidence_datatype)
            )]
        return []

    def _add_doc_concrete_type_if_present(self, doc_type_dict: Dict, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        # Check if type is present in documentType
        if Config.type_field in doc_type_dict and doc_type_dict[Config.type_field]:
            return [(
                # Add type to documentType
                self.make_object_node(f"documentType-{paper[Config.paper_id_field]}"),
                self.make_predicate_node(Config.type_predicate),
                self.make_literal_node(doc_type_dict[Config.type_field], Config.type_datatype)
            )]
        return []

    def _create_doc_type_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            # Add documentType to enrichment
            self.make_object_node(f"enrichment-{paper[Config.paper_id_field]}"),
            self.make_predicate_node(Config.doc_type_predicate),
            self.make_object_node(f"documentType-{paper[Config.paper_id_field]}")
        ), (
            # Add type to documentType
            self.make_object_node(f"documentType-{paper[Config.paper_id_field]}"),
            self.make_type_predicate_node(),
            self.make_class_type_node(Config.doc_type_class)
        )]

    def _add_references_if_present(self, enrichments_dict: Dict, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        # Check if references are present
        if Config.references_field in enrichments_dict and enrichments_dict[Config.references_field]:
            return [(
                # Add references to enrichment
                self.make_object_node(f"enrichment-{paper[Config.paper_id_field]}"),
                self.make_predicate_node(Config.references_predicate),
                self.make_literal_node(reference, Config.references_datatype)
            ) for reference in enrichments_dict[Config.references_field]]
        return []

    def _create_enrichment_triple(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            # Link paper to enrichment
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.enrichments_predicate),
            self.make_object_node(f"enrichment-{paper[Config.paper_id_field]}")
        ), (
            # Add type to enrichment
            self.make_object_node(f"enrichment-{paper[Config.paper_id_field]}"),
            self.make_type_predicate_node(),
            self.make_class_type_node(Config.enrichments_class)
        )]

    def _extraction_field(self) -> str:
        return Config.enrichments_field


class LanguageParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing DocumentTypes"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing DocumentTypes"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        language_dict = paper[Config.language_field]
        if any([
            Config.language_code_field in language_dict,
            Config.language_name_field in language_dict,
        ]):
            triples = self._create_language_triples(paper)
            triples += self._add_language_code_if_present(language_dict, paper)
            triples += self._add_language_name_if_present(language_dict, paper)
        return triples

    def _create_language_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            # Add documentLanguage to paper
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.language_predicate),
            self.make_object_node(f"documentLanguage-{paper[Config.paper_id_field]}")
        ), (
            # Add type to documentLanguage
            self.make_object_node(f"documentLanguage-{paper[Config.paper_id_field]}"),
            self.make_type_predicate_node(),
            self.make_class_type_node(Config.language_class)
        )]

    def _add_language_code_if_present(self, language_dict: Dict, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        # Check if code is present in language
        if Config.language_code_field in language_dict and language_dict[Config.language_code_field]:
            return [(
                # Add code to language
                self.make_object_node(f"documentLanguage-{paper[Config.paper_id_field]}"),
                self.make_predicate_node(Config.language_code_predicate),
                self.make_literal_node(language_dict[Config.language_code_field], Config.language_code_datatype)
            )]
        return []

    def _add_language_name_if_present(self, language_dict: Dict, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        # Check if name is present in language
        if Config.language_name_field in language_dict and language_dict[Config.language_name_field]:
            return [(
                # Add name to language
                self.make_object_node(f"documentLanguage-{paper[Config.paper_id_field]}"),
                self.make_predicate_node(Config.language_name_predicate),
                self.make_literal_node(language_dict[Config.language_name_field], Config.language_name_datatype)
            )]
        return []

    def _extraction_field(self) -> str:
        return Config.language_field


class PublisherParser(MappingParser):
    publishers_set: Set = set()

    def _start_parsing_message(self) -> str:
        return f"Started parsing Publishers"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Publishers"

    @staticmethod
    def _clean_publisher_name(publisher_name: str) -> str:
        if publisher_name.endswith("'") and publisher_name.startswith("'"):
            return publisher_name[1:-1]
        return publisher_name

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        triples = []
        triples += [
            # Add connection to paper
            (
                self.make_paper_subject(paper[Config.paper_id_field]),
                self.make_predicate_node(Config.publisher_predicate),
                self.make_object_node(f"publisher-{abs(hash(paper[Config.publisher_field]))}")
            )
        ]
        if paper[Config.publisher_field] not in self.publishers_set:
            triples += [
                # Add label to publisher
                (
                    self.make_object_node(f"publisher-{abs(hash(paper[Config.publisher_field]))}"),
                    self.make_label_predicate_node(),
                    self.make_literal_node(self._clean_publisher_name(paper[Config.publisher_field]))
                ),
                # Add type to publisher
                (
                    self.make_object_node(f"publisher-{abs(hash(paper[Config.publisher_field]))}"),
                    self.make_type_predicate_node(),
                    self.make_class_type_node(Config.publisher_class)
                )
            ]
            self.publishers_set.add(paper[Config.publisher_field])
        return triples

    def _extraction_field(self) -> str:
        return Config.publisher_field


class RelationsParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing Relations"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Relations"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.relations_predicate),
            self.make_literal_node(relation, Config.relations_datatype)
        ) for relation in paper[Config.relations_field]]

    def _extraction_field(self) -> str:
        return Config.relations_field


class ContributorsParser(MappingParser):

    def _start_parsing_message(self) -> str:
        return f"Started parsing Contributors"

    def _end_parsing_message(self) -> str:
        return f"Finished parsing Contributors"

    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        return [(
            self.make_paper_subject(paper[Config.paper_id_field]),
            self.make_predicate_node(Config.contributors_predicate),
            self.make_literal_node(contributor, Config.contributors_datatype)
        ) for contributor in paper[Config.contributors_field]]

    def _extraction_field(self) -> str:
        return Config.contributors_field
