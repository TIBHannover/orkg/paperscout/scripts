import json
import logging
import os
from abc import ABC, abstractmethod
from typing import Optional, Dict, List, Tuple, Any

from rdflib import Graph, RDF, RDFS
from rdflib.term import Node, URIRef, Literal

from mapping.config import Config

logger = logging.getLogger(__name__)


class MappingParser(ABC):

    def parse(self, file: str) -> Graph:
        graph: Graph = Graph()
        if os.path.exists(file) and os.path.isfile(file):
            logger.debug(self._start_parsing_message())
            with open(file, 'r') as json_file:
                json_data = json.load(json_file)
                if isinstance(json_data, Dict):
                    logger.warning(f"File {file} contains a dict not a list")
                    return graph
                for paper in json_data:
                    if (Config.paper_id_field in paper
                            and self._extraction_field() in paper
                            and paper[self._extraction_field()]):
                        for triple in self._generate_triples(paper):
                            graph.add(triple)
            logger.debug(self._end_parsing_message())
        return graph

    @abstractmethod
    def _generate_triples(self, paper: Dict) -> List[Tuple[Node, Node, Node]]:
        pass

    @abstractmethod
    def _start_parsing_message(self) -> str:
        pass

    @abstractmethod
    def _end_parsing_message(self) -> str:
        pass

    @abstractmethod
    def _extraction_field(self) -> str:
        pass

    @staticmethod
    def make_paper_subject(paper_id: str) -> URIRef:
        return URIRef(f"{Config.base_url}entity/paper-{paper_id}")

    @staticmethod
    def make_predicate_node(predicate_id: str) -> URIRef:
        return URIRef(f"{Config.base_url}property/{predicate_id}")

    @staticmethod
    def make_type_predicate_node() -> URIRef:
        return URIRef(RDF.type)

    @staticmethod
    def make_label_predicate_node() -> URIRef:
        return URIRef(RDFS.label)

    @staticmethod
    def make_class_type_node(class_id: str) -> URIRef:
        return URIRef(f"{Config.base_url}entity/{class_id}")

    @staticmethod
    def make_object_node(object_id: str) -> URIRef:
        return URIRef(f"{Config.base_url}entity/{object_id}")

    @staticmethod
    def make_literal_node(literal_value: Any, datatype: Optional[str] = None) -> Literal:
        if datatype is None:
            return Literal(literal_value)
        else:
            return Literal(literal_value, datatype=URIRef(datatype))
