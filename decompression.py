import json
import os
import tarfile

from tqdm import tqdm

NUM_OF_LINES = 120_000
DELETE = True


def unpack_and_process(folder_path: str, output_path: str):
    tar_files = [f for f in os.listdir(folder_path) if f.endswith('.tar.xz')]
    idx = 438
    json_data = []

    temp_dir = os.path.join(folder_path, 'temp')
    if not os.path.exists(temp_dir):
        os.mkdir(temp_dir)

    for tf in tqdm(tar_files):
        try:
            tf_path = os.path.join(folder_path, tf)
            tar = tarfile.open(tf_path)
            tar.extractall(temp_dir)
            tar.close()

            if DELETE:
                os.remove(tf_path)

            for root, dirs, files in os.walk(temp_dir):
                for f in files:
                    if f.endswith('.json'):
                        fpath = os.path.join(root, f)
                        with open(fpath) as json_file:
                            data = json.load(json_file)
                            json_data.append(data)

            if len(json_data) >= NUM_OF_LINES:
                with open(os.path.join(output_path, f"output{idx}.json"), "w") as f:
                    json.dump(json_data, f)
                    idx += 1
                    json_data = []
        except Exception as e:
            print("Error: ", e)
            continue
        finally:
            if DELETE:
                os.system(f"rm -r {temp_dir}/*")


if __name__ == "__main__":
    unpack_and_process("./core2021", "./json")
