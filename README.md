# PaperScout scripts
This repository contains scripts for the PaperScout project.

## Data preparation

First things first, you need to download the CORE dataset, and decompress it. You can download the dataset from [here](https://core.ac.uk/services/dataset/). The dataset is around 400GB compressed and 2TB uncompressed.

Make sure you have python installed on your environment. Python 3.11 is recommended.
Next you should install the requirements using the following command:
```bash
$ pip install -r requirements.txt
```


Two scripts are used for the data preparation:
* `decompression.py` - This script is used after you download the CORE dataset AND decompress it. After the first decompression you will end up with many archive files and this script will aggregate the results into JSONL files to work with INodes limitations on disk space.
* `mapper.py` - This script is used to map the JSON files into RDF files. It is used to map the CORE dataset into RDF files. The mapper leverages `morghkgc` package to do the conversion based on the RML definitions found in `mappings.ttl` file.

Before running the mapper scripts you will need to convert the jsonl files to json files containing an array of JSON objects. This can be done with the following script:
```bash
$ chmod +x ./convert_jsonl_to_json.sh & ./convert_jsonl_to_json.sh <path_to_jsonl_files>
```

**Note:** Running the mapping script requires the json data to be placed in a file named `research_papers.json`. This will automatically be created if you use the `mapper.py` script.

### Alternative mapping process
The `mapper.py` script relies on RML mappings which though are powerful, they are still limited to what they can do in certain cases. Hence, we developed our own custom mapping script that is not as flexible as the declarative language but are able to handle more complex cases.

Before calling the script, you can adapt any variable needed in the `mapping/config.py` file.
Once you are done, you can run the script using the following command:
```bash
$ python mapping/custom.py
```

## GraphDB
Since the official image of GraphDB will not allow to run the pre-load script, we need to build our own image. This is based on the official image with the addition of the pre-load script. The Dockerfile is based on the following [repository](https://github.com/Ontotext-AD/graphdb-docker/tree/167af9eb5503c903c5e39a090a5aef94787350f9).

### Pre-load
GraphDB offers a variety of ways to load data and the fastest is the preload method using the `importRDF` tool. (See [here](https://graphdb.ontotext.com/documentation/10.0/loading-data.html) for more information).

The following files are required for this task:
* `Dockerfile`: This is the Dockerfile used to build the image for the pre-loading of the data.
* `.env`: This file contains the environment variables used by the Dockerfile.
* `docker-compose.yml`: This file contains the configuration for the docker-compose tool in an easier way.

The `Dockerfile` need to be updated with the repository name after you create it. If not you will need to update the files and add a repository config file to the env and created one in ttl format.

To build the docker image you can use the following command:
```bash
docker build --no-cache --pull --build-arg version=10.4.1 -t ontotext/graphdb:10.4.1-amd64 .
```

**Note:** Use `.env.example` to create your `.env` file, and make sure that the version used in the build command matches what you have in the `.env` file.

Next you run the pre-load image via a simple command, and you wait for the import to finish.
```bash
docker compose -f docker-compose-preload.yml up
```

### Server load
If the pre-load is not working, you can use the server load method. This is slower than the pre-load method but it will work. 

You can run the engine via docker compose using the following command:
```bash
docker compose up -d
```

### Stable version
Once you finish the import, you need to stop your pre-load image of graph db and revert back to the stable version. This can be done by running the following command:
```bash
docker compose up -d
```

## Time requirements
These are approximate time requirements for the scripts which depend on resources available. All the resources listed need to be available beside whatever is used for other tasks.
* 1st Decompression: 2h 30m - (12 cores, 400GB disk HDD)
* 2nd Decompression: 24h  - (12 cores, 2TB disk HDD)
* Conversion: 6h - (12 cores, 1.6TB disk HDD)
* Mapping: 36h - (min 100GB RAM, 12 cores, 1.6TB disk HDD) 
* Alternative Mapping: ???
* Pre-load into GraphDB: 14h - (min 90GB RAM, 6 cores, 2.2TB disk HDD)
* Server load into GraphDB (alternative if writing index is broken): 70h - (min 90GB RAM, 1 core, 2.6TB disk HDD)

## Hardware Requirements
This recommendation is based on the time requirements and the resources used during the process which is done mainly on the CORE-2021 dataset (400+GB compressed, 2TB semi-uncompressed).

|            | Minimum    | Recommended |
|------------|------------|-------------|
| **CPU**    | 12 cores   | 24 cores    |
| **Memory** | 128 GB RAM | 256 GB RAM  |
| **Disk**   | 8 TB HDD   | 20 TB SSD   |

