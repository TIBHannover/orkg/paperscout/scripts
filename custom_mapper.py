import os

from tqdm import tqdm

from mapping.parsers.base import Config
from mapping.parsers.paper import PaperParser, DOIParser, OAIParser, TitleParser, DatePublishedParser, AbstractParser
from mapping.parsers.paper import YearParser, ISSNParser, FullTextParser, SubjectsParser, URLsParser, IdentifiersParser
from mapping.parsers.paper import TopicsParser, DownloadURLParser, FullTextIdentifierParser, PDFHashValueParser
from mapping.parsers.paper import RawRecordXMLParser, JournalsParser, AuthorsParser, EnrichmentsParser, LanguageParser
from mapping.parsers.paper import PublisherParser, RelationsParser, ContributorsParser
from mapping.custom import Dispatcher


def set_up_dispatcher(output_file: str = "output.nt") -> Dispatcher:
    dispatcher = Dispatcher(output_file)
    dispatcher.add_parser(PaperParser())
    dispatcher.add_parser(DOIParser())
    dispatcher.add_parser(OAIParser())
    dispatcher.add_parser(TitleParser())
    dispatcher.add_parser(DatePublishedParser())
    dispatcher.add_parser(AbstractParser())
    dispatcher.add_parser(YearParser())
    dispatcher.add_parser(ISSNParser())
    dispatcher.add_parser(FullTextParser())
    dispatcher.add_parser(SubjectsParser())
    dispatcher.add_parser(URLsParser())
    dispatcher.add_parser(IdentifiersParser())
    dispatcher.add_parser(TopicsParser())
    dispatcher.add_parser(DownloadURLParser())
    dispatcher.add_parser(FullTextIdentifierParser())
    dispatcher.add_parser(PDFHashValueParser())
    dispatcher.add_parser(RawRecordXMLParser())
    dispatcher.add_parser(JournalsParser())
    dispatcher.add_parser(AuthorsParser())
    dispatcher.add_parser(EnrichmentsParser())
    dispatcher.add_parser(LanguageParser())
    dispatcher.add_parser(PublisherParser())
    dispatcher.add_parser(RelationsParser())
    dispatcher.add_parser(ContributorsParser())
    return dispatcher


def concat_content_of_file(input_file: str, output_directory: str) -> None:
    """
    Concatenate the content of the file with the knowledge-graph.nt file
    :return: Nothing
    """
    global file_num
    global times
    with open(f"{output_directory}core-{str(file_num)}.nt", "a+") as destination, open(input_file, "r") as source:
        destination.write(source.read())
        destination.write("\n")
    times += 1
    if times >= num_files_written:
        file_num += 1
        times = 0


times = 14
num_files_written = 100
file_num = 0
output_directory = "./rdf/"
DELETE = True
Config.process_num = 4
Config.output_format = "nt"

files = [f"./json/{file}" for file in os.listdir("./json")]
files = sorted(files, key=os.path.getsize)
interm_file = "./output.nt"
dispatcher = set_up_dispatcher(interm_file)
for file in tqdm(files, unit="files"):
    if file.endswith(".json"):
        file_path = file
        Config.input_file = file_path
        try:
            dispatcher.run()
            concat_content_of_file(interm_file, output_directory)
            # delete the file
            if DELETE:
                os.remove(file_path)
            os.remove(interm_file)
        except Exception as ex:
            continue
